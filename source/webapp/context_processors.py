from django.urls import translate_url
from django.conf import settings

def stats(request):
    return {
        'times': request.session.get('page_times', {}),
        'visits': request.session.get('page_visits', {}),
        'times_total': request.session.get('times_total', 0),
        'visits_total': request.session.get('visits_total', 0)
    }


def strip_language_code(request):
    return {
        'default_language': settings.LANGUAGE_CODE,
        'language_base_link': translate_url(request.path, settings.LANGUAGE_CODE)
    }
